import { signIn } from "next-auth/react";
import { getSession } from "next-auth/react";
import { GetSessionParams } from "next-auth/react";
import { Button, Flex, Heading, Stack, Text } from "@chakra-ui/react";

export default function Login() {
  return (
    <Flex align="center" justify="center" minH="100vh">
      <Stack w="30rem" p={8} rounded="md" boxShadow="md">
        <Heading textAlign="center" fontSize="2xl" marginBottom="4">
          Isomorphic Git for Postgres
        </Heading>
        <Button onClick={() => signIn("gitlab")} mt={4}>
          <Text>Sign in using GitLab</Text>
        </Button>
      </Stack>
    </Flex>
  );
}
