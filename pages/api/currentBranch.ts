import fs from "fs";
import path from "path";
import git from "isomorphic-git";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const dir = path.join(process.cwd(), "postgres-clone");

  await git
    .currentBranch({
      fs,
      dir,
      fullname: false,
    })
    .then((current) => {
      res.status(200).json(current);
    })
    .catch((e: unknown) => {
      if (e instanceof Error) {
        res.status(400).json({ error: e.message });
      }
    });
}
