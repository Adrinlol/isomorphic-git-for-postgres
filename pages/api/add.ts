import fs from "fs";
import path from "path";
import git from "isomorphic-git";
import http from "isomorphic-git/http/node";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { filepath, filecontent, username, password, message } = JSON.parse(
    req.body
  );

  const dir = path.join(process.cwd(), "postgres-clone");
  const file = path.join(dir, filepath);

  if (fs.existsSync(dir)) {
    try {
      fs.appendFileSync(file, filecontent);
      await git.add({ fs, dir: "./postgres-clone", filepath });
    } catch (e) {
      console.log("git.add error", e);
    }
  }

  await git.commit({
    fs,
    dir,
    message: message,
    author: {
      name: username,
      email: password,
    },
  });

  try {
    await git
      .push({
        fs,
        dir,
        http,
        remote: "origin",
        ref: "master",
        onAuth: () => ({
          username: username,
          password: password,
        }),
      })
      .then(() => {
        res.status(200).json({ success: true });
      });
  } catch (e: unknown) {
    if (e instanceof Error) {
      res.status(400).json({ error: e.message });
    }
  }
}
