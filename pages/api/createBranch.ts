import fs from "fs";
import path from "path";
import git from "isomorphic-git";
import http from "isomorphic-git/http/node";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { ref, username, password } = JSON.parse(req.body);
  const dir = path.join(process.cwd(), "postgres-clone");

  await git.branch({
    fs,
    dir,
    ref: ref,
    checkout: true,
  });

  try {
    await git
      .push({
        fs,
        dir,
        http,
        remote: "origin",
        onAuth: () => ({
          username: username,
          password: password,
        }),
      })
      .then(() => {
        res.status(200).json({ success: true });
      });
  } catch (e: unknown) {
    if (e instanceof Error) {
      res.status(400).json({ error: e.message });
    }
  }
}
