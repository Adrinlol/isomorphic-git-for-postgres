import NextAuth from "next-auth";
import GitlabProvider from "next-auth/providers/gitlab";

export default NextAuth({
  providers: [
    GitlabProvider({
      clientId: String(process.env.GITLAB_CLIENT_ID),
      clientSecret: String(process.env.GITLAB_CLIENT_SECRET),
    }),
  ],
  theme: {
    colorScheme: "light",
  },
  secret: process.env.NEXTAUTH_SECRET,
});
