import fs from "fs";
import { join } from "path";
import { simpleGit } from "simple-git";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { repoUrl } = JSON.parse(req.body);
  const target = join(process.cwd(), "postgres-clone");

  if (fs.existsSync("./postgres-clone")) {
    try {
      fs.rmSync("./postgres-clone", { recursive: true });
    } catch (e) {
      console.log("fs.rmSync error", e);
    }
  }

  await simpleGit()
    .clone(repoUrl, target)
    .cwd({ path: target })
    .then(() => {
      simpleGit().add("./postgres-clone");
      res.status(200).json({ success: true });
    })
    .catch((e: unknown) => {
      if (e instanceof Error) {
        res.status(400).json({ error: e.message });
      }
    });
}
