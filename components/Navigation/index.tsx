import { signOut } from "next-auth/react";
import { Flex, Text, Button } from "@chakra-ui/react";

export const Navigation = () => {
  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="1.5rem"
    >
      <Text fontWeight="bold">Isomorphic Git for Postgres</Text>
      <Button onClick={() => signOut()}>Sign out</Button>
    </Flex>
  );
};
