import { useState } from "react";
import {
  Box,
  Button,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
} from "@chakra-ui/react";

export const Branches = ({ currentBranch }: { currentBranch: string }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [branchName, setBranchName] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const createBranch = async () => {
    setIsLoading(true);
    await fetch("api/createBranch", {
      method: "POST",
      body: JSON.stringify({
        ref: branchName,
        username: username,
        password: password,
      }),
    }).then(async () => {
      setIsOpen(false);
      setIsLoading(false);
    });
  };

  const handleClose = () => {
    setUsername("");
    setPassword("");
    setIsOpen(false);
  };

  return (
    <Box>
      <Text marginBottom={"2"}>
        {currentBranch} is the name of the branch currently pointed to by
        .git/HEAD
      </Text>
      <FormLabel fontSize="lg" marginTop={"8"}>
        Create new branch
      </FormLabel>
      <Box display="flex" gap="10px">
        <Input
          required
          variant="outline"
          placeholder="New branch name"
          value={branchName}
          onChange={(e) => setBranchName(e.target.value)}
        />
        <Button onClick={() => setIsOpen(true)} isDisabled={!branchName}>
          Create
        </Button>
      </Box>
      <Modal isOpen={isOpen} onClose={handleClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create new branch</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Input
              mb={4}
              required
              variant="outline"
              placeholder="Your username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <Input
              type="password"
              required
              variant="outline"
              placeholder="Your password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </ModalBody>
          <ModalFooter>
            <Button variant="ghost" mr={3} onClick={handleClose}>
              Close
            </Button>
            <Button
              colorScheme="blue"
              onClick={createBranch}
              isLoading={isLoading}
              isDisabled={!username || !password}
            >
              Create branch
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
};
