import { useState } from "react";
import { Box, Input, Button, FormControl, FormLabel } from "@chakra-ui/react";

import { Branches } from "@/components/Branches";

export const Clone = () => {
  const [repoUrl, setRepoUrl] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [currentBranch, setCurrentBranch] = useState("");

  const asyncClone = async () => {
    setIsLoading(true);
    await fetch("api/clone", {
      method: "POST",
      body: JSON.stringify({
        repoUrl: repoUrl,
      }),
    })
      .then(async () => {
        setIsLoading(false);
      })
      .then(async (fetchedClone) => {
        if (fetchedClone !== undefined) {
          await fetch("api/currentBranch").then(async (res) => {
            setIsLoading(false);
            setCurrentBranch(await res.json());
          });
        }
      });
  };

  return (
    <Box width="100%" maxWidth="600px" margin="0 auto">
      <FormControl isRequired display="flex" flexDirection="column" mb="8">
        <FormLabel>Gitlab Repository URL</FormLabel>
        <Box display="flex" gap="10px">
          <Input
            required
            variant="outline"
            placeholder="Gitlab Repo URL"
            value={repoUrl}
            onChange={(e) => setRepoUrl(e.target.value)}
          />
          <Button
            onClick={asyncClone}
            isLoading={isLoading}
            isDisabled={!repoUrl}
          >
            Clone
          </Button>
        </Box>
      </FormControl>
      {currentBranch && <Branches currentBranch={currentBranch} />}
    </Box>
  );
};
